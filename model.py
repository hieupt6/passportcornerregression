import os
from custom_keras_layers import *
from datetime import datetime
from dataset import Dataset
from hparams import hparams


import tensorflow as tf
import math
import numpy as np
import logging
logging.basicConfig(level=logging.DEBUG)


class Model(object):
    def __init__(self):
        self.best_val_loss = math.inf
        # dataset
        self.train_dataset = Dataset(hparams, hparams.train_record_path)
        self.valid_dataset = Dataset(hparams, hparams.valid_record_path)
        self.train_dataset.load_tfrecord()
        self.valid_dataset.load_tfrecord()

    def loss_function(self, real, pred):
        loss = self.loss_object(real, pred)
        return loss

    # def quadrant_angle(self, labels):
        # x_cord = [labels[:, 0] * hparams.max_width, labels[:, 2] * hparams.max_width,
        #   labels[:, 4] * hparams.max_width, labels[:, 6] * hparams.max_width]
        # y_cord = [labels[:, 1] * hparams.max_height, labels[:, 3] * hparams.max_height,
        #   labels[:, 5] * hparams.max_height, labels[:, 7] * hparams.max_height]

        # angle = np.arctan2(y_cord, x_cord) * 180.0 / np.pi
        # normalize to range (0,360)
        # mask = (angle < 0)
        # angle[mask] += 360
        # mask = (angle > 360)
        # angle[mask] -= 360
        # return angle / 360.0

    def create_model(self):
        # create model
        self.model = ConvBaseLayer(hparams)
        # define training ops and params
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=hparams.lr)
        # from_logits=True, reduction='none'
        self.loss_object = tf.keras.losses.MeanSquaredError()

        self.last_epoch = 0
        self.train_summary_writer = tf.summary.create_file_writer(
            hparams.save_path + '/logs/train')
        self.valid_summary_writer = tf.summary.create_file_writer(
            hparams.save_path + '/logs/valid')
        self.checkpoint_dir = os.path.join(hparams.save_path, 'train')
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, "ckpt")
        self.checkpoint = tf.train.Checkpoint(optimizer=self.optimizer,
                                              model=self.model)

    def load_model(self):
        if hparams.restore_cp_path != None:
            logging.info('load model from {}'.format(hparams.restore_cp_path))
            self.checkpoint.restore(hparams.restore_cp_path)
            self.optimizer = tf.keras.optimizers.Adam(
                learning_rate=hparams.restore_lr)
        else:
            latest = tf.train.latest_checkpoint(self.checkpoint_dir)
            if latest != None:
                logging.info('load model from {}'.format(latest))
                self.last_epoch = int(latest.split('-')[-1])
                self.checkpoint.restore(latest)

    def train_step(self, batch_input, batch_target):
        loss = 0
        with tf.GradientTape() as tape:
            predictions = self.model(batch_input)
            loss += self.loss_function(batch_target, predictions)

        variables = self.model.trainable_variables
        gradients = tape.gradient(loss, variables)
        self.optimizer.apply_gradients(zip(gradients, variables))
        return loss / int(batch_target.shape[0])

    def evaluate(self, batch_input, batch_target):
        predictions = self.model(batch_input)
        mse_loss = self.loss_function(predictions, batch_target)
        return mse_loss / int(batch_target.shape[0])

    def inference(self, image):
        result = self.model(np.array([image]))
        return result

    def train(self):
        self.load_model()
        for epoch in range(self.last_epoch, hparams.max_epochs):
            start = datetime.now()
            total_loss = 0
            # train each batch in dataset

            for batch, (batch_input, batch_target) in enumerate(self.train_dataset.dataset):
                batch_loss = self.train_step(batch_input, batch_target)
                total_loss += batch_loss

                if batch % 1 == 0:
                    print('Epoch {} Batch {} Loss {:.4f}'.format(
                        epoch + 1, batch+1, batch_loss.numpy()))
            total_loss = total_loss/(batch+1)
            total_loss_val = 0

            for batch, (batch_input, batch_target) in enumerate(self.valid_dataset.dataset):
                batch_loss = self.evaluate(batch_input, batch_target)
                total_loss_val += batch_loss
            total_loss_val = total_loss_val/(batch+1)

            # save checkpoint
            if hparams.save_best:
                if self.best_val_loss > total_loss_val:
                    self.checkpoint.save(file_prefix=self.checkpoint_prefix)
                    self.best_val_loss = total_loss_val
            else:
                self.checkpoint.save(file_prefix=self.checkpoint_prefix)

            # write log
            with self.train_summary_writer.as_default():
                tf.summary.scalar('Loss train', total_loss, step=epoch)

            with self.valid_summary_writer.as_default():
                tf.summary.scalar('loss valid', total_loss_val, step=epoch)

            # log traing result of each epoch
            logging.info('Epoch {} Loss {:.6f}'.format(
                epoch + 1, total_loss))
            logging.info('Loss on valid set: {:.6f}'.format(total_loss_val))
            logging.info('Time taken for 1 epoch {} sec\n'.format(
                datetime.now() - start))
