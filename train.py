import os
# Use "-1" for no GPU, "0" for the first GPU and "1" for second GPU, etc
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
from model import Model


gpus = tf.config.list_physical_devices('GPU')
print(gpus)
if len(gpus) > 0:
    tf.config.experimental.set_memory_growth(gpus[0], True)
print("TFversion: ", tf.__version__)


def main():
    # create model
    model = Model()
    model.create_model()
    # train model
    model.train()


if __name__ == '__main__':
    main()
