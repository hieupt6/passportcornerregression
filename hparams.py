
class Hparams:
    def __init__(self):
        # data and save path
        self.train_record_path = '../dataset/passport_record_cLy_filtered/passport/PP_Dect.train'
        self.num_train_sample = 48335
        self.valid_record_path = '../dataset/passport_record_cLy_filtered/passport/PP_Dect.valid'
        self.num_valid_sample = 2948
        # Place to save checkpoints and training logs
        self.save_path = 'training_checkpoints'
        # Whether or not save only the best result on valid set or save checkpoint at each epoch
        self.save_best = False

        # Restore training from a checkpoint
        # Should change the save_path to another path
        self.restore_cp_path = None
        self.restore_lr = 0.00001

        # input params
        # (400, 400, 3)
        self.max_width = 400
        self.max_height = 400
        self.num_point = 4

        # conv_tower params
        # base model from tf.keras.application, or custom instance of tf.keras.Model
        # check for new models from https://www.tensorflow.org/api_docs/python/tf/keras/applications
        self.base_model_name = 'EfficientNetB1'
        # last convolution layer from base model which extract features from
        # inception v3: mixed2 (mixed_5d in tf.slim inceptionv3)
        # inception resnet v2: (mixed_6a in tf.slim inception_resnet_v2)
        self.end_point = 'block7b_se_squeeze'

        self.dense_units = 256
        # training params
        self.batch_size = 1
        self.max_epochs = 400
        self.lr = 0.0001  # Tuning hyperparameters


hparams = Hparams()
