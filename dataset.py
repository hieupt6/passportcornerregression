import tensorflow as tf
from hparams import hparams


class Dataset(object):
    def __init__(self, hparams, record_path):
        self.hparams = hparams
        self.record_path = record_path
        self.keys_to_features = {
            'image/encoded': tf.io.FixedLenFeature([], tf.string, default_value=''),
            'image/class': tf.io.FixedLenFeature([hparams.num_point*2], tf.float32)
        }

    def parse_tfrecord(self, example):
        res = tf.io.parse_single_example(example, self.keys_to_features)
        image = tf.cast(tf.io.decode_image(res['image/encoded'], 3), tf.float32)
        label = res['image/class']
        return image, label

    def load_tfrecord(self):
        dataset = tf.data.TFRecordDataset(self.record_path)
        dataset.shuffle(self.hparams.num_train_sample)
        dataset = dataset.map(self.parse_tfrecord)
        self.dataset = dataset.batch(self.hparams.batch_size)
        self.dataset.prefetch(tf.data.experimental.AUTOTUNE)
        self.iterator = iter(dataset)

    def next_batch(self):
        return self.iterator.get_next()
