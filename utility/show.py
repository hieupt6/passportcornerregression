import os
import cv2
import argparse


'''
Plot images in a folder ang theirs corresponding label
'''


def draw_dot(image, points):
    for i, p in enumerate(points):
        color = (0, 0, 255)
        cv2.putText(image, str(i), tuple(map(int, tuple(p))),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL, 3, color, 2)
        cv2.circle(image, tuple(map(int, tuple(p))), 4, color, -1)
    return image


def show(image):
    h, w, _ = image.shape
    image = cv2.resize(image, (600, int(h * 600 / w)))
    cv2.imshow("IMAGE", image)
    cv2.waitKey(0)
    cv2.destroyWindow("IMAGE")


def main(args):
    filename = os.listdir(args.folder_path)
    for file in filename:
        if file.__contains__('.txt'):
            continue
        path = os.path.join(args.folder_path, file)
        img = cv2.imread(path)
        with open(path.split('.')[0] + '.txt', 'r') as f:
            for line in f:
                line = line.split(',')
                tl, tr, br, bl = (float(line[0]), float(line[1])), (float(line[2]), float(line[3])), (
                    float(line[4]), float(line[5])), (float(line[6]), float(line[7]))
        coords = [tl, tr, br, bl]
        print(file)
        print(coords)
        h, w, _ = img.shape
        coords = [(int(w * pt[0]), int(h * pt[1])) for pt in coords]
        img = draw_dot(img, coords)
        show(img)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--folder_path', help='path to the folder which stores images and labels')
    args = parser.parse_args()
    main(args)
