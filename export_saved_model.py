import tensorflow as tf
import numpy as np
import argparse
from model import Model
from hparams import hparams

def set_tensorflow_config(per_process_gpu_memory_fraction=0.95):
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
    config.gpu_options.allow_growth=True
    sess = tf.compat.v1.Session(config=config)

def main(args):
    model = Model()
    model.create_model()
    model.checkpoint.restore(args.ckpt)
    model.inference(np.random.uniform(
        1, 255, (hparams.max_height, hparams.max_width, 3)))
    tf.saved_model.save(model.model, args.sm)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ckpt', default='training_checkpoints/train/ckpt-34', help='path to trained ckpt')
    parser.add_argument('--sm', default='savedmodel',
                        help='path to saved_model')
    args = parser.parse_args()
    set_tensorflow_config()
    main(args)