import cv2
import numpy as np
import tensorflow as tf
import argparse


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    pts = np.float32(pts)
    (tl, tr, br, bl) = pts

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(pts, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped


def main(args):
    # load saved model
    model = tf.saved_model.load(args.saved_model_path)
    # inference
    num_point = 4
    img = cv2.imread(args.img_path)
    h, w, _ = img.shape
    rs_img = cv2.resize(img, (400, 400))
    input_tensor = tf.convert_to_tensor(np.array([rs_img]), dtype=tf.float32)

    # predict
    result = model.signatures['serving_default'](input_tensor)['fill_later'][0]

    list_pts = [int(float(p) * w) if i % 2 == 0 else int(float(p) * h)
                for i, p in enumerate(result)]
    pts = [(list_pts[i], list_pts[i + 1]) for i in range(0, num_point * 2, 2)]
    pts = np.array(pts)
    cut_image = four_point_transform(img, pts)
    cv2.imwrite('cut_img.jpg', cut_image)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--img_path', help='path to an image')
    parser.add_argument('--saved_model_path', help='path to savedmodel')
    args = parser.parse_args()
    main(args)
