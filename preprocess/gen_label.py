import cv2
import os
import argparse

'''
The code will generate labels for ROI images which have already been cut
'''


def main(args):
    for name in os.listdir(args.folder_path):
        fp = os.path.join(args.folder_path, name)
        img = cv2.imread(fp)
        if img is None:
            continue
        lp = os.path.join(args.folder_path, name.split('.')[0] + '.txt')
        with open(lp, 'w') as f:
            f.write(
                '0.003333,0.006667,0.991667,0.005000,0.995000,0.993333,0.005000,0.993333,')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder_path', help='path to the ROI folder')
    args = parser.parse_args()
    main(args)
