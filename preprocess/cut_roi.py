import os
import cv2
import numpy as np
import argparse


'''
Cut ROI from images according to labels
'''


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    pts = np.float32(pts)
    (tl, tr, br, bl) = pts

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(pts, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped


def main(args):
    filename = [file for file in os.listdir(args.input_path) if file.endswith('.txt') == False]
    for i, file in enumerate(filename):
        print(i, file)
        path = os.path.join(args.input_path, file)
        img = cv2.imread(path)
        with open(path.split('.')[0] + '.txt', 'r') as f:
            for line in f:
                line = line.split(',')
                tl, tr, br, bl = (float(line[0]), float(line[1])), (float(line[2]), float(line[3])), (
                float(line[4]), float(line[5])), (float(line[6]), float(line[7]))
        coords = [tl, tr, br, bl]
        h, w, _ = img.shape
        coords = [(int(w * pt[0]), int(h * pt[1])) for pt in coords]
        cut_img = four_point_transform(img, coords)
        cv2.imwrite(os.path.join(args.save_path, file), cut_img)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_path', help='path to the folder which stores images and labels')
    parser.add_argument('--save_path')
    args = parser.parse_args()
    main(args)