from fake_functions import *
import numpy as np
import argparse
import cv2
import os
import random


'''
Generate fake data based on original images, labels and arbitrary backgrounds
'''


def read_label(label_file):
    with open(label_file, 'r') as f:
        for line in f:
            line = line.split(',')
            tl, tr, br, bl = (float(line[0]), float(line[1])), (float(line[2]), float(
                line[3])), (float(line[4]), float(line[5])), (float(line[6]), float(line[7]))
    coords = [tl, tr, br, bl]
    return np.array(coords)


def write_label(path, coords):
    '''
    coords: 1-D array with format x1, y1, x2, y2, ...
    '''
    with open(path, 'w') as f:
        f.write('%f,%f,%f,%f,%f,%f,%f,%f,' % tuple(coords.ravel()))


def main(args):
    bgs_p = [os.path.join(args.bg_path, name)
             for name in os.listdir(args.bg_path)]
    img_paths = [file for file in os.listdir(args.inp_path) if file.endswith('.txt') == False]
    for image_file_name in img_paths:
        im = cv2.imread(os.path.join(args.inp_path, image_file_name))
        if im is None:
            continue

        coords = read_label(os.path.join(
            args.inp_path, image_file_name.split('.')[0]+'.txt'))

        for functionname in functions.keys():
            print(image_file_name.split('.')[0] + '_' + functionname + '.jpg')
            bg = cv2.imread(random.choice(bgs_p))
            img, new_coords = functions[functionname](im.copy(), coords, bg)
            cv2.imwrite(os.path.join(args.out_path, image_file_name.split('.')[0] + '_' + functionname + '.jpg'), img)
            save_path_label = os.path.join(args.out_path, image_file_name.split('.')[0] + '_' + functionname + '.txt')
            write_label(save_path_label, new_coords)
        cv2.imwrite(os.path.join(args.out_path, image_file_name.split('.')[0] + '_roi' + '.jpg'), im)
        save_path_label = os.path.join(
            args.out_path, image_file_name.split('.')[0] + '_roi' + '.txt')
        write_label(save_path_label, coords)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--inp_path")
    parser.add_argument("--out_path")
    parser.add_argument("--bg_path", help="Path to the background folder")
    args = parser.parse_args()
    main(args)
