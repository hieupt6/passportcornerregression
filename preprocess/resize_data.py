import argparse
import cv2
import os
import shutil


def main(args):
    image_ls = [file for file in os.listdir(args.raw_data) if file.endswith('.txt') == False]

    new_w = 400
    new_h = 400

    for i, image_path in enumerate(image_ls):
        print(i, image_path)
        im = cv2.imread(os.path.join(args.raw_data, image_path))
        if im is None:
            continue
        im = cv2.resize(im, (new_w, new_h))
        cv2.imwrite(os.path.join(args.pad_data, image_path), im)
        shutil.copy(os.path.join(args.raw_data, image_path.split('.')[0]+'.txt'), os.path.join(args.pad_data, image_path.split('.')[0]+'.txt'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--raw_data")
    parser.add_argument("--pad_data")
    args = parser.parse_args()
    main(args)
