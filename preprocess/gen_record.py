import tensorflow as tf
import argparse
import cv2
import os


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _int64_list_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def _float_list_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def serialize_example(image_encoded, image_class):
    feature = {
        'image/encoded': _bytes_feature(image_encoded),
        'image/class': _float_list_feature(image_class)
    }
  
    # Create a Features message using tf.train.Example.
  
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto.SerializeToString()

def main(args):
    save_path = args.out_path
    filenames = [os.path.join(args.pad_path, file) for file in os.listdir(args.pad_path) if file.endswith('.txt') == False]
    cnt = 0
    error_files = []
    with tf.io.TFRecordWriter(save_path) as writer:
        for i, filename in enumerate(filenames):
            print(i, filename)
            try:
                #image/encoded
                image_encoded = []
                with open(filename, "rb") as f:
                    image_encoded = f.read()
                # Try to read
                image_height, image_width, _ = cv2.imread(filename).shape
                
                # image/class
                with open(filename.split('.')[0] + '.txt', 'r') as f:
                    image_class = f.read().split(',')[:8]
                    image_class = [float(p) for p in image_class]
                    print(image_class)
                # write to TFRecordFile
                example = serialize_example(image_encoded, image_class)
                writer.write(example)
                cnt += 1
            except Exception as e:
                error_files.append((filename, e))
            
    print("Total number of samples written:", cnt)
    if len(error_files)>0:
        print("Error samples:")
        for file, error in error_files:
            print(file, '|', error)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--pad_path", help = "directory contain images", default = "/home/local/VNG/Invoice OCR/code/data/train_images_pad")
    parser.add_argument("--out_path", help = "output path", default = "/home/local/VNG/Invoice OCR/code/data/train_images_pad/data.train")
    args = parser.parse_args()
    main(args)
