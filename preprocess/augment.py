import os
import argparse
import cv2
import imgaug
import imgaug.augmenters as iaa
import numpy as np
from imgaug.augmentables import Keypoint, KeypointsOnImage
from gen_fake_data import write_label, read_label
from fake_functions import order_points


'''
Augment data X times
Let A is the number of original samples. After augmenting, the augment folder have (X+1)*A samples which include original A samples
'''


augment = iaa.SomeOf((0, 3), [
    iaa.Add((-40, 40)),
    iaa.AdditiveGaussianNoise(scale=(0, 0.1 * 255), per_channel=True),
    iaa.Multiply((0.7, 1.3)),
    iaa.MultiplyElementwise((0.7, 1.3), per_channel=0.5),
    iaa.CoarseDropout(0.02, size_percent=(0.2, 0.5), per_channel=0.5),
    iaa.SaltAndPepper(0.15),
    iaa.SaltAndPepper(0.15, per_channel=True),
    iaa.JpegCompression(compression=(20, 40)),
    iaa.GaussianBlur(sigma=(0.0, 1.5)),
    iaa.MotionBlur(k=3),
    iaa.WithBrightnessChannels(iaa.Add((-30, 30))),
    iaa.GammaContrast((0.5, 1.5)),
    # iaa.Affine(scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}),
    iaa.Affine(translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)}),
    iaa.Affine(rotate=(-15, 15)),
    iaa.Affine(shear=(-15, 15)),
    iaa.PiecewiseAffine(scale=(0.01, 0.03)),
    iaa.WithPolarWarping(iaa.CropAndPad(percent=(-0.02, 0.02))),
    iaa.imgcorruptlike.Fog(severity=1)
], random_order=True)


def main(args):
    img_ls = [file for file in os.listdir(
        args.inp_folder) if file.endswith('.txt') == False]
    cnt = 0
    for name in img_ls:
        image = cv2.imread(os.path.join(args.inp_folder, name))
        if image is None:
            continue
        #Resize first for fast augment, you can choose whether to use it or not
        image = cv2.resize(image, (400, 400))
        cnt += 1
        print(cnt, name)
        coords = read_label(os.path.join(
            args.inp_folder, name.split('.')[0]+'.txt'))
        cv2.imwrite(os.path.join(args.augment_folder, name), image)
        write_label(os.path.join(args.augment_folder,
                    name.split('.')[0]+'.txt'), coords)
        for _ in range(int(args.X)):
            h, w, _ = image.shape
            new_coords = [(w * pt[0], h * pt[1]) for pt in coords]
            kps = KeypointsOnImage([
                Keypoint(x=new_coords[0][0], y=new_coords[0][1]),
                Keypoint(x=new_coords[1][0], y=new_coords[1][1]),
                Keypoint(x=new_coords[2][0], y=new_coords[2][1]),
                Keypoint(x=new_coords[3][0], y=new_coords[3][1])
            ], shape=image.shape)

            # Augment keypoints and images.
            image_aug, kps_aug = augment(image=image, keypoints=kps)

            new_coords = [(kp.x, kp.y) for kp in kps_aug.keypoints]
            new_coords = np.array(new_coords)
            new_coords = order_points(new_coords)
            new_coords = [(pt[0]/image_aug.shape[1],pt[1]/image_aug.shape[0]) for pt in new_coords]
            new_coords = np.array(new_coords)
            if (new_coords >= 1).sum() + (new_coords <= 0).sum() > 0:
                continue
            uid = np.random.randint(99999999)
            sp = os.path.join(args.augment_folder, name.split('.')[0] + '_%d.jpg' % uid)
            cnt += 1
            print(cnt, sp)
            cv2.imwrite(sp, image_aug)
            write_label(sp.replace('.jpg', '.txt'), new_coords)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--inp_folder')
    parser.add_argument('--augment_folder')
    parser.add_argument(
        '--X', help="augment X times the original number of samples")
    args = parser.parse_args()
    main(args)
