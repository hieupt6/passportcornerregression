import random
import cv2
import numpy as np


def rotate_bound(image, angle, pts):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # perform the actual rotation and return the image

    # inverse matrix of simple rotation is reversed rotation.
    M_inv = cv2.getRotationMatrix2D((cX, cY), angle * (-1), 1)
    M_inv[0, 2] += (nW / 2) - cX
    M_inv[1, 2] += (nH / 2) - cY
    # points
    pts = np.array(pts)
    # add ones
    ones = np.ones(shape=(len(pts), 1))

    points_ones = np.hstack([pts, ones])

    # transform points
    transformed_points = M_inv.dot(points_ones.T).T

    return cv2.warpAffine(image, M, (nW, nH)), transformed_points


def paste(src, dst, pts):
    # paste the roi on the background
    h_s, w_s = src.shape[:2]
    h_d, w_d = dst.shape[:2]
    new_tl_corner = (random.randint(0, w_d - w_s),
                     random.randint(0, h_d - h_s))
    pts = [(point[0] + new_tl_corner[0], point[1] + new_tl_corner[1])
           for point in pts]
    mask = np.array(src > 0, dtype=np.uint8)
    dst[new_tl_corner[1]:new_tl_corner[1] + h_s, new_tl_corner[0]:new_tl_corner[0] + w_s, :] = dst[new_tl_corner[1]:
                                                                                                   new_tl_corner[
                                                                                                       1] + h_s,
                                                                                                   new_tl_corner[0]:
                                                                                                   new_tl_corner[0] + w_s,
                                                                                                   :] * (
        1 - mask) + src * mask
    pts = np.array(pts)
    return dst, pts


def order_points(pts):
    # Reorder points
    pts = np.array(pts)
    rect = np.zeros((4, 2), dtype="float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[1] = pts[(np.argmin(s) + 1) % 4]
    rect[2] = pts[(np.argmin(s) + 2) % 4]
    rect[3] = pts[(np.argmin(s) + 3) % 4]
    return rect  # tl, tr, br, bl


def get_random_coords(w_bg, h_bg):
    part_w = w_bg // 10
    part_h = h_bg // 10
    tl = (np.random.randint(0, 2.5 * part_w),
          np.random.randint(0, 2.5 * part_h))
    tr = (np.random.randint(7.5 * part_w, w_bg),
          np.random.randint(0, 2.5 * part_h))
    br = (np.random.randint(7.5 * part_w, w_bg),
          np.random.randint(7.5 * part_h, h_bg))
    bl = (np.random.randint(0, 2.5 * part_w),
          np.random.randint(7.5 * part_h, h_bg))
    coords = np.array([tl, tr, br, bl])
    return coords


def morph_paste(src, dst, bg_size):
    dst_coords = get_random_coords(bg_size[0], bg_size[1])
    h_s, w_s = src.shape[:2]
    h_d, w_d = dst.shape[:2]
    src_coords = np.array([
        [0, 0], [w_s - 1, 0], [w_s - 1, h_s - 1], [0, h_s - 1]], dtype="float32")

    trans, _ = cv2.findHomography(src_coords, dst_coords)

    dst_map = cv2.warpPerspective(src, trans, (h_d, w_d))
    mask = np.array(dst_map > 0, dtype=np.uint8)
    res = dst * (1 - mask) + dst_map * mask

    return res, dst_coords.astype('int')

# -----------------------------------------------------------------------------------------------------------


def rotate_paste(img, coords, bg):
    h, w, _ = img.shape
    coords = [(int(w * pt[0]), int(h * pt[1])) for pt in coords]
    res, coords = rotate_bound(img, random.randint(0, 359), coords)
    res_w, res_h, _ = res.shape
    alpha = random.uniform(1.1, 2.2)
    bg_size = (int(max(res_w, res_h) * alpha),
               int(max(res_w, res_h) * alpha))
    bg = cv2.resize(bg, bg_size)
    # paste
    res, coords = paste(res, bg.copy(), coords)
    coords = order_points(coords)
    coords = coords/bg_size
    return res, coords


def rotate90x_morph(img, coords, bg):
    h, w, _ = img.shape
    coords = [(int(w * pt[0]), int(h * pt[1])) for pt in coords]
    bg_size = (400, 400)
    bg = cv2.resize(bg, bg_size)
    prob = random.randint(0, 3)
    if prob == 0:
        pass
    elif prob == 1:
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    elif prob == 2:
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    else:
        img = cv2.rotate(img, cv2.ROTATE_180)
    res, coords = morph_paste(img, bg, bg_size)
    coords = coords/bg_size[0]
    return res, coords

functions = {
    'rotate_paste': rotate_paste,
    'rotate90x_morph': rotate90x_morph
}
