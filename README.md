## Introduction: Passport Corner Regression
---

The model finds four corner points of an passport image.

![Model Demo](https://drive.google.com/uc?export=view&id=1rWSdcMuaCLi17xulF7a3igWd8ozmUNtA)

The label file of an image is a text file with coordinates of four corner points **top-left, top-right, bottom_right and bottom_left** sequentially

*pt0_x, pt0_y, pt1_x, pt1_y, pt2_x, pt2_y, pt3_x, pt3_y,*

## Data preparation
---

1. Cut ROI from original image for generating fake data.
```shell
$ python3 preprocess/cut_roi.py --input_path=/home/path/to/image/and/label/folder --save_path/home/path/to/save/folder/
$ python3 preprocess/gen_label.py --folder_path=/home/path/to/ROI/folder
```
2. Generate fake data.
```shell
$ python3 preprocess/gen_fake_data.py --inp_path=/home/ROI/folder --out_path=/home/path/to/save/fake/data --bg_path=/home/background/path/to/generate/data/on
$ copy -a /home/path/to/original/data/folder/. /home/path/to/save/fake/data/
```
3. Augment data
```
$ python3 preprocess/augment.py --inp_folder=/home/path/to/save/fake/data --augment_folder=/home/path/to/save/augment/data --X=2
```
4. Resize images and generate tfrecord file.
```shell
$ python3 preprocess/resize_data.py --raw_data=/home/augment/data --pad_data=/home/resized/data
$ python3 preprocess/gen_record.py --pad_path=/home/resized/data --out_path=data.train
```

**WARNING: Don't use current folder('.') or parent folder("..") symbols when passing arguments**

## Configuration
---
Hyper parameters are saved in *hparams.py*. You need to specify:

- train_record_path: path to data.train
- num_train_sample
- valid_record_path: path to data.valid
- num_valid_sample
- base_model_name
- end_point: last layer of the chosen base model as endpoint of CNN block
- batch_size
- lr: learning rate

## Train
---
```shell
$ python3 train.py
```

## Inference
---
```shell
$ python3 inference.py --img_path=/path/to/image --saved_model_path=/path/to/saved/model
```
